#!/bin/bash

# Set the php configuration that we want to change
echo "upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}" >> /usr/local/etc/php/conf.d/piwigo.ini
echo "post_max_size = ${PHP_POST_MAX_SIZE}" >> /usr/local/etc/php/conf.d/piwigo.ini
echo "max_file_uploads = ${PHP_MAX_FILE_UPLOADS}" >> /usr/local/etc/php/conf.d/piwigo.ini

# Set Apache timeout
sed -i s/^Timeout.*/Timeout\ ${APACHE_TIMEOUT}/  /etc/apache2/apache2.conf

# Mount the s3 filesystem for storage
if [ ! -z $AWS_ACCESS_KEY_ID ]; then
  # Allow non-root users to mout fuse filesystems
  sed -i'' 's/^# *user_allow_other/user_allow_other/' /etc/fuse.conf

  mkdir -p /var/www/html/galleries
  chown www-data:www-data /var/www/html/galleries
  su -s /bin/bash -c 'yas3fs --nonempty s3://${GALLERY_BUCKET}/${GALLERY_PATH} /var/www/html/galleries' www-data

  mkdir -p /var/www/html/upload
  chown www-data:www-data /var/www/html/upload
  su -s /bin/bash -c 'yas3fs --nonempty s3://${UPLOAD_BUCKET}/${UPLOAD_PATH} /var/www/html/upload' www-data

  mkdir -p /var/www/html/_data/i
  chown www-data:www-data /var/www/html/_data/i
  su -s /bin/bash -c 'yas3fs --nonempty s3://${CACHE_BUCKET}/${CACHE_PATH} /var/www/html/_data/i' www-data
fi

# Run the docker entrypoint that came with the official php container
docker-php-entrypoint $@

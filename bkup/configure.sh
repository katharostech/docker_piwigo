#!/bin/bash

# Set the php configuration that we want to change
sed -i s/^upload_max_filesize.*/upload_max_filesize\ =\ ${PHP_UPLOAD_MAX_FILESIZE}/  /etc/php/7.0/apache2/php.ini 
sed -i s/^post_max_size.*/post_max_size\ =\ ${PHP_POST_MAX_SIZE}/  /etc/php/7.0/apache2/php.ini 
sed -i s/^max_file_uploads.*/max_file_uploads\ =\ ${PHP_MAX_FILE_UPLOADS}/  /etc/php/7.0/apache2/php.ini 

# Set Apache timeout
sed -i s/^Timeout.*/Timeout\ ${APACHE_TIMEOUT}/  /etc/apache2/apache2.conf

# Mount the s3 filesystem for storage
if [ -n $AWS_ACCESS_KEY_ID ]; then
  yas3fs s3://${GALLERY_BUCKET}/${GALLERY_PATH} /var/www/galleries
  yas3fs s3://${UPLOAD_BUCKET}/${UPLOAD_PATH} /var/www/upload
  yas3fs s3://${CACHE_BUCKET}/${CACHE_PATH} /var/www/_data/i
fi

/entrypoint.sh

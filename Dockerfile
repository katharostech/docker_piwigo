FROM php:7.1-apache

# Set default for environment variables used to configure the container
ENV AWS_ACCESS_KEY_ID ""
ENV AWS_SECRET_ACCESS_KEY ""
ENV PHP_UPLOAD_MAX_FILESIZE "2M"
ENV PHP_POST_MAX_SIZE "8M"
ENV PHP_MAX_FILE_UPLOADS "20"
ENV APACHE_TIMEOUT "300"
ENV GALLERY_BUCKET ""
ENV GALLERY_PATH ""
ENV UPLOAD_BUCKET ""
ENV UPLOAD_PATH ""
ENV CACHE_BUCKET ""
ENV CACHE_PATH ""

# Install dependencies
RUN set -x && \
    apt-get update && \
    apt-get install -y busybox ffmpeg fuse imagemagick python-pip unzip && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pip install yas3fs

# Add a convenience alias for vi
RUN echo "alias vi='busybox vi'" >> /root/.bashrc

# Install piwigo
RUN set -x && \
    curl http://piwigo.org/download/dlcounter.php?code=latest > /tmp/piwigo.zip && \
    unzip -q /tmp/piwigo.zip && \
    mv piwigo/* . && \
    rm -rf piwigo && \
    rm -rf /tmp/piwigo.zip

# Install php extensions
RUN set -x && \
    apt-get update && \
    apt-get install -y libpng-dev && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp* && \
    docker-php-ext-install -j$(nproc) exif gd mysqli && \
    apt-get remove -y libpng-dev && \
    apt-get autoremove -y

# Copy in custom entrypoint
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod 744 /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["apache2-foreground"]

